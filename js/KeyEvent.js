var KeyEvent = {};

     //   ==============================KeyEvent==================================
     /** Kconstey code constant: Unknown key code. */
     KEYCODE_UNKNOWN = 0;
     /** Key code constant: Soft Left key.
      * Usually situated below the display on phones and used as a multi-function
      * feature key for selecting a software defined function shown on the bottom left
      * of the display. */
     const KEYCODE_SOFT_LEFT = 1;
     /** Key code constant: Soft Right key.
      * Usually situated below the display on phones and used as a multi-function
      * feature key for selecting a software defined function shown on the bottom right
      * of the display. */
     const KEYCODE_SOFT_RIGHT = 2;
     /** Key code constant: Home key.
      * This key is handled by the framework and is never delivered to applications. */
     const KEYCODE_HOME = 3;
     /** Key code constant: Back key. */
     const KEYCODE_BACK = 4;
     /** Key code constant: Call key. */
     const KEYCODE_CALL = 5;
     /** Key code constant: End Call key. */
     const KEYCODE_ENDCALL = 6;
     /** Key code constant: '0' key. */
     const KEYCODE_0 = 7;
     /** Key code constant: '1' key. */
     const KEYCODE_1 = 8;
     /** Key code constant: '2' key. */
     const KEYCODE_2 = 9;
     /** Key code constant: '3' key. */
     const KEYCODE_3 = 10;
     /** Key code constant: '4' key. */
     const KEYCODE_4 = 11;
     /** Key code constant: '5' key. */
     const KEYCODE_5 = 12;
     /** Key code constant: '6' key. */
     const KEYCODE_6 = 13;
     /** Key code constant: '7' key. */
     const KEYCODE_7 = 14;
     /** Key code constant: '8' key. */
     const KEYCODE_8 = 15;
     /** Key code constant: '9' key. */
     const KEYCODE_9 = 16;
     /** Key code constant: '*' key. */
     const KEYCODE_STAR = 17;
     /** Key code constant: '#' key. */
     const KEYCODE_POUND = 18;
     /** Key code constant: Directional Pad Up key.
      * May also be synthesized from trackball motions. */
     const KEYCODE_DPAD_UP = 19;
     /** Key code constant: Directional Pad Down key.
      * May also be synthesized from trackball motions. */
     const KEYCODE_DPAD_DOWN = 20;
     /** Key code constant: Directional Pad Left key.
      * May also be synthesized from trackball motions. */
     const KEYCODE_DPAD_LEFT = 21;
     /** Key code constant: Directional Pad Right key.
      * May also be synthesized from trackball motions. */
     const KEYCODE_DPAD_RIGHT = 22;
     /** Key code constant: Directional Pad Center key.
      * May also be synthesized from trackball motions. */
     const KEYCODE_DPAD_CENTER = 23;
     /** Key code constant: Volume Up key.
      * Adjusts the speaker volume up. */
     const KEYCODE_VOLUME_UP = 24;
     /** Key code constant: Volume Down key.
      * Adjusts the speaker volume down. */
     const KEYCODE_VOLUME_DOWN = 25;
     /** Key code constant: Power key. */
     const KEYCODE_POWER = 26;
     /** Key code constant: Camera key.
      * Used to launch a camera application or take pictures. */
     const KEYCODE_CAMERA = 27;
     /** Key code constant: Clear key. */
     const KEYCODE_CLEAR = 28;
     
     /**
      * Called when a key was pressed down and not handled by any of the views inside of the activity. 
      * So, for example, key presses while the cursor is inside a TextView will not trigger the event 
      * (unless it is a navigation to another object) because TextView handles its own key presses.
      * If the focused view didn't want this event, this method is called.
      * Other additional default key handling may be performed if configured with setDefaultKeyMode. 
      * Returns:
      * Return true to prevent this event from being propagated further, or false to indicate that you 
      * have not handled this event and it should continue to be propagated.
      * See Also:
      * onKeyUp, KeyEvent
      */
     function onZDANKeyDown (keyCode) {
         //TODO
         ZDANWindow.finishWindow();
     }
     
     
     /**
      * Called when a key was released and not handled by any of the views inside of the activity.
      *  So, for example, key presses while the cursor is inside a TextView will not trigger the event
      *  (unless it is a navigation to another object) because TextView handles its own key presses.
      * The default implementation handles KEYCODE_BACK to stop the activity and go back.
      * Returns:
      * Return true to prevent this event from being propagated further, or false to indicate that
      * you have not handled this event and it should continue to be propagated.
      * See Also:
      * onKeyDown, KeyEvent
      */
     function onZDANKeyUp (keyCode) {
         //TODO
         ZDANWindow.finishWindow();
     }
     
     
     /**
      * Default implementation of {}: always returns false (doesn't handle
      * the event).
      *
      * To receive this callback, you must return true from onKeyDown for the current
      * event stream.
      */
     function onZDANKeyLongPress(keyCode) {
         //TODO
         ZDANWindow.finishWindow();
     }