var ZDANWindow = {};


    /**
     * finish window .
     */
    finishWindow = function () {
        ZDANWindow.finishWindow();
    }
    
    /**
     * set window title.
     */
     setTitle = function (title) {
        ZDANWindow.setTitle(title);
    }
    
    
    /**
    * toast error message
    */
    errorTips = function (message) {
        ZDANWindow.errorTips(message);
    }
    
    /**
    * toast success message
    */
    successTips = function (message) {
        ZDANWindow.successTips(message);
    }
    
    /**
    * toast info message
    */
    infoTips = function (message) {
        ZDANWindow.infoTips(message);
    }
    
    /**
    * toast warnin message
    */
    warningTips = function (message) {
        ZDANWindow.warningTips(message);
    }
    
    
    /**
    * toast normal message
    */
    normalTips = function (message) {
        ZDANWindow.normalTips(message);
    }
    
    /**
     * 
     * dialog message
     */
    dialog = function (title,message,determineMessage,method) {
        ZDANWindow.dialog(title,message,determineMessage,method +'');
    }
    